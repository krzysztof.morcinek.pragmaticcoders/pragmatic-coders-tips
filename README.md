# pragmatic-coders-tips

## Nie działa kamera w laptopach MSI. 

* Któryś z przycisków FN/ALT sa zamienione wiec kamere sie sprzętowo włącza poprzez kombinacje czegoś z wymienionych i F6(F6 jest odpowiedzialny za kamerę)
* sprawdz czy nie jest blokowana przez chrome https://support.google.com/hangouts/thread/2640241?hl=en

## beautiful json file from command line

* https://stackoverflow.com/questions/352098/how-can-i-pretty-print-json-in-a-shell-script using python: `$ python -m json.tool my_json.json`, `$ python -m json.tool my_json.json > nice.json`

## Nagrywanie Monday Talk (from Linux)

* Simple Screen recorder (instalowanie: https://vitux.com/record-your-screen-with-simple-screen-recorder-under-ubuntu/)
    * czy tak jak jest opisane by default mi działało? - Polecam doczytać do końca i poustawiać.
* po odpaleniu konferencji upewnić się że jestem zmutowany.
* Odpal `$ caffeine` - blokuje zeby screensaver się nie uruchomił (a to sie zdarzało gdy odpaliłem na kompie w pracy i szedłem do kuchni na talka)
* Upewnić się że jestem podpięty do zasilania
* mogę wybrać który screen "Record the entire screen" i wybieramy który.
* "Start Recording"
  * upewnić się, że prowadzący już prezentuje Screen. Gdy zacznie prezentację później, recorder może tego nie złapać (zdarzało się)
* Brak kodeków, wrzucić na google drive i tam już widać. (jeśli zaznaczę MP4 i MP3 to będę widział przed uploadem na Drive):
  * ![simple screen recorder settings](images/simple-screen-recorder-settings.png "simple screen recorder settings") 
* w jakim katalogu pojawia się nagranie?
  * gdy rozpoczynam nagrywać to po kliknięciu "Continue" mam File->"Save as" i wybiera się plik. podczas nagrywania dodaje się data do tego pliku
* Wrzucić na Monday Talk folder: 'https://drive.google.com/drive/u/0/folders/1vv828nuXPTgWenuL4QkpGYobQRlfWvD5'
  * obrabianie godzinnego pliku trwa po stronie drive'a, więc od wrzucenia do możliwości sprawdzenia co jest mija >1h.
  * propozycja nazywania: "monday-talk-###data###-###imie_nazwisko###.mp4", "monday-talk-2020_11_23-jakub_noga.mp4"

## Rozszerzenie swap (domyślnie jest tylko 1GB) a ponoć ubuntu wiesza się tylko gdy się kończy pamięć.

* https://linuxize.com/post/how-to-add-swap-space-on-ubuntu-18-04/ - wszystko krok po kroku jak jest tutaj.

## Zamiana klawiszy FN i Win na klawiaturze laptopa MSI

* https://forum-en.msi.com/index.php?topic=305734.0 w scroll do "Win / Fn key swap in bios."

## Ubuntu tricks

* Screenshot z zaznaczeniem: SHIFT+Prt Scrn.