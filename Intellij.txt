# working with intellij

* to to Terminal: Press Alt+F12 .
* Continue debugging: F9

Intellij: idea:
ALT+1 przenosi mnie do ProjectView.


ALT+SHIFT UP/DOWN przy przeszukiwaniu.

Kopiowanie do schowka działa z CTRL+Insert.

F2
Shift+F2
Navigate between code issues
Jump to the next or previous highlighted error.

Gdy są problemy w Idea Vim zabierającym skróty to w ustawieniach (Vim Emulation) tych skrótów trzeba zaznaczyc żeby Idea a nie VIM handlował je.

https://www.jetbrains.com/help/rider/Navigation_and_Search__Bookmarks.html :
Bookmarks with mnemonics — are most useful for marking code lines that you ofter refer to, like top 10 places in your current solution. You can have up to 10 numbered bookmarks simultaneously. They are indicated by the following icons on the left editor gutter: bookmarkNumber

Use dedicated shortcuts for this kind of bookmarks: Ctrl+Shift+<digit> toggles the bookmark with this digit; Ctrl+<digit> navigates to the bookmark with this digit.

CTRL+B przenosi nas do używania.

CTRL + C działa w Idea.
Alt + Shift + ← (Left Arrow)


ctrl- : zwija aktualną metodę.

ctrl SHIFT - : zwija wszystko w pliku.

ALT + F12 to open terminal.
ESC to go back to editing file.